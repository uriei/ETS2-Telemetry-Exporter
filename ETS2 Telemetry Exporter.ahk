#SingleInstance force

;Reading last settings from config file.
IniRead, TelemetryUrl, %A_ScriptDir%\config.ini, Main, TelemetryUrl
IniRead, loopdelay, %A_ScriptDir%\config.ini, Main, LoopDelay
IniRead, CheckTime, %A_ScriptDir%\config.ini, Data, Time
IniRead, CheckTruckDamage, %A_ScriptDir%\config.ini, Data, TruckDamage
IniRead, CheckETA, %A_ScriptDir%\config.ini, Data, ETA
IniRead, CheckJob, %A_ScriptDir%\config.ini, Data, Job
IniRead, CheckOdo, %A_ScriptDir%\config.ini, Data, Odometer
IniRead, CheckTruck, %A_ScriptDir%\config.ini, Data, TruckInfo
IniRead, CheckFuel, %A_ScriptDir%\config.ini, Data, FuelInfo
IniRead, CheckDelete, %A_ScriptDir%\config.ini, Main, Delete
IniRead, MDistance, %A_ScriptDir%\config.ini, Measurement, Distance
IniRead, MWeight, %A_ScriptDir%\config.ini, Measurement, Weight
IniRead, MCurrency, %A_ScriptDir%\config.ini, Measurement, Currency

;Setting Defaults in case of no previously saved config.
if (TelemetryURL = "ERROR")
	TelemetryURL := "http://localhost:25555/api/ets2/telemetry"
if (loopdelay = "ERROR")
	loopdelay := 5
if (MDistance = "ERROR")
	MDistance := 1
if (MWeight = "ERROR")
	MWeight := 1
if (MCurrency = "ERROR")
	MCurrency := 1

;Emptying folder
FileDelete, %A_ScriptDir%\Files\*.txt

;Creating GUI
Gui, Add, Text, x12 y9 w350 h20, Please enter your Telemetry API URL as shown on the Telemetry server:
Gui, Add, Edit, x12 y39 w350 h20 vTelemetryUrl, %TelemetryUrl%

Gui, Add, Text, x12 y89 w140 h20, Select update frequency
;Creating Loop Radial options with the previous one already checked.
if (loopdelay = 1){
		Gui, Add, Radio, x12 y119 w100 h30 vloopdelay1 Checked, 1 second
	} else {
		Gui, Add, Radio, x12 y119 w100 h30 vloopdelay1, 1 second
	}
if (loopdelay = 10){
		Gui, Add, Radio, x252 y119 w100 h30 vloopdelay10 Checked, 10 seconds
	} else {
		Gui, Add, Radio, x252 y119 w100 h30 vloopdelay10, 10 seconds
	}
if (loopdelay = 5){
		Gui, Add, Radio, x132 y119 w100 h30 vloopdelay5 Checked, 5 seconds
	} else {
		Gui, Add, Radio, x132 y119 w100 h30 vloopdelay5, 5 seconds
	}	

Gui, Add, Text, x12 y169 w350 h20 , Choose which data you want to export:
Gui, Add, CheckBox, x12 y199 w100 h30 vCheckTime Checked%CheckTime%, Current Time (in-game)
Gui, Add, CheckBox, x132 y199 w100 h30 vCheckTruckDamage Checked%CheckTruckDamage%, Truck and Trailer Damage
Gui, Add, CheckBox, x252 y199 w100 h30 vCheckETA Checked%CheckETA%, ETA
Gui, Add, CheckBox, x12 y239 w100 h30 vCheckJob Checked%CheckJob%, Job Info
Gui, Add, CheckBox, x132 y239 w100 h30 vCheckOdo Checked%CheckOdo%, Odometer
Gui, Add, CheckBox, x252 y239 w100 h30 vCheckTruck Checked%CheckTruck%, Truck Info
Gui, Add, CheckBox, x12 y279 w100 h30 vCheckFuel Checked%CheckFuel%, Fuel Info

Gui, Add, Text, x12 y329 w350 h20 , Choose your prefered measurement/currency system:

Gui, Add, Text, x12 y359 w100 h20 , Distance
if (MDistance = 1){
		Gui, Add, Radio, x12 y379 w100 h20 vMDistance Checked, Kilometers
		Gui, Add, Radio, x12 y409 w100 h20 , Miles
	} else {
		Gui, Add, Radio, x12 y379 w100 h20 vMDistance, Kilometers
		Gui, Add, Radio, x12 y409 w100 h20 Checked, Miles
	}

Gui, Add, Text, x132 y359 w100 h20 , Weight
if (MWeight = 1){
		Gui, Add, Radio, x132 y379 w100 h20 vMWeight Checked, Kilograms
		Gui, Add, Radio, x132 y409 w100 h20 , Pounds
	} else {
		Gui, Add, Radio, x132 y379 w100 h20 vMWeight, Kilograms
		Gui, Add, Radio, x132 y409 w100 h20  Checked, Pounds
	}

Gui, Add, Text, x252 y359 w100 h20 , Currency
if (MCurrency = 1){
		Gui, Add, Radio, x252 y379 w100 h20 vMCurrency Checked, Euros
		Gui, Add, Radio, x252 y409 w100 h20 , Pounds
	} else {
		Gui, Add, Radio, x252 y379 w100 h20 vMCurrency, Euros
		Gui, Add, Radio, x252 y409 w100 h20  Checked, Pounds
	}

Gui, Add, CheckBox, x12 y439 w340 h20 vCheckDelete Checked%CheckDelete%, Delete files when closing

Gui, Add, Button, x122 y479 w120 h40 Default, Confirm
Gui, Show, x647 y213 h537 w386, ETS2 Telemetry Exporter
return

ButtonConfirm:
Gui, Submit, NoHide
;If no option was selected
if ((CheckTime+CheckTruckDamage+CheckETA+CheckJob+CheckOdo+CheckTruck+CheckFuel)=0){
	MsgBox, 5, Error, You haven't selected any data to export
	IfMsgBox Retry
		return
	Else
		Goto, GuiClose
}
Gui, Destroy

if loopdelay1
	loopdelay := 1
if loopdelay5
	loopdelay := 5
if loopdelay10
	loopdelay := 10
	
loopdelaystep := loopdelay*4 
if (TelemetryURL = "")
	TelemetryURL := "http://localhost:25555/api/ets2/telemetry"
	
;Saving settings
IniWrite, %TelemetryUrl%, %A_ScriptDir%\config.ini, Main, TelemetryUrl
IniWrite, %loopdelay%, %A_ScriptDir%\config.ini, Main, LoopDelay

IniWrite, %CheckTime%, %A_ScriptDir%\config.ini, Data, Time
IniWrite, %CheckTruckDamage%, %A_ScriptDir%\config.ini, Data, TruckDamage
IniWrite, %CheckETA%, %A_ScriptDir%\config.ini, Data, ETA
IniWrite, %CheckJob%, %A_ScriptDir%\config.ini, Data, Job
IniWrite, %CheckOdo%, %A_ScriptDir%\config.ini, Data, Odometer
IniWrite, %CheckTruck%, %A_ScriptDir%\config.ini, Data, TruckInfo
IniWrite, %CheckFuel%, %A_ScriptDir%\config.ini, Data, FuelInfo
IniWrite, %CheckDelete%, %A_ScriptDir%\config.ini, Main, Delete
IniWrite, %MDistance%, %A_ScriptDir%\config.ini, Measurement, Distance
IniWrite, %MWeight%, %A_ScriptDir%\config.ini, Measurement, Weight
IniWrite, %MCurrency%, %A_ScriptDir%\config.ini, Measurement, Currency

;Creating new GUI.
Gui, Add, Progress, x12 y49 w320 h10 cBlue BackgroundGray Range1-%loopdelaystep% -Smooth vDelayBar, 0
Gui, Add, Text, x12 y9 w320 h30 , Done`, now you just need to keep this window open.`nYou can minimize it.
Gui, Add, Text, x12 y79 w320 h30 , You can change your measurement preferences while the exporter is running:

Gui, Add, Text, x12 y129 w100 h20 , Distance
if (MDistance = 1){
		Gui, Add, Radio, x12 y149 w100 h30 vMDistance Checked, Kilometers
		Gui, Add, Radio, x12 y179 w100 h30 , Miles
	} else {
		Gui, Add, Radio, x12 y149 w100 h30 vMDistance, Kilometers
		Gui, Add, Radio, x12 y179 w100 h30 Checked, Miles
	}
Gui, Add, Text, x122 y129 w100 h20 , Weight
if (MWeight = 1){
		Gui, Add, Radio, x122 y149 w100 h30 vMWeight Checked, Kilograms
		Gui, Add, Radio, x122 y179 w100 h30 , Pounds
	} else {
		Gui, Add, Radio, x122 y149 w100 h30 vMWeight, Kilograms
		Gui, Add, Radio, x122 y179 w100 h30  Checked, Pounds
	}
Gui, Add, Text, x232 y129 w100 h20 , Currency
if (MCurrency = 1){
		Gui, Add, Radio, x232 y149 w100 h30 vMCurrency Checked, Euros
		Gui, Add, Radio, x232 y179 w100 h30 , Pounds
	} else {
		Gui, Add, Radio, x232 y149 w100 h30 vMCurrency, Euros
		Gui, Add, Radio, x232 y179 w100 h30  Checked, Pounds
	}

Gui, Add, Button, x122 y229 w100 h30 Default, Exit
Gui, Show, x649 y302 h276 w349, ETS2 Telemetry Exporter

;Creating folder if doesn't exists
FileCreateDir, files

;Starting Loop
Loop {
	;Resetting progressbar
	GuiControl,, DelayBar, 0

	;Downloading the data
	Loop {
		;Check if the data can be downloaded, if so, downloads it
		try {
			whr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
			whr.Open("GET", TelemetryUrl, true)
			whr.Send()
			whr.WaitForResponse()
			TelemetryData := whr.ResponseText
		}
		;If the data can't be downloaded...
		catch {
			TelemetryDownloadErrorCount++
			If (TelemetryDownloadErrorCount > 4) {
				MsgBox, 5, Error, ETS2 Exporter failed 5 times at getting Telemetry Data from Telemetry Server, please, check the entered URL is correct and server is running.
				IfMsgBox Retry
					Reload
				Else
					Goto, GuiClose
			} else continue
			
		}
		;If it could be downloaded, reset Error counter and leave the retrying loop
		TelemetryDownloadErrorCount:=0
		break
	}

	;Exporting Data to files
	;Current Time
	if CheckTime{
		CurrentTimeIndex := InStr(TelemetryData,"time")
		CurrentTimeValue := SubStr(TelemetryData,CurrentTimeIndex+7,16)
		CurrentTimeHourValue := SubStr(TelemetryData,CurrentTimeIndex+18,2)
		CurrentTimeMinuteValue := SubStr(TelemetryData,CurrentTimeIndex+21,2)
		FileDelete, %A_ScriptDir%\Files\CurrentTime.txt
		FileAppend, %CurrentTimeHourValue%:%CurrentTimeMinuteValue%, %A_ScriptDir%\Files\CurrentTime.txt
	}

	;Fuel Info
	if CheckFuel{
		;Current Fuel level
		CurrentFuelIndex := InStr(TelemetryData,"fuel")
		CurrentFuelIndexEnd := InStr(TelemetryData,",",false,CurrentFuelIndex)
		CurrentFuelIndexCount := CurrentFuelIndexEnd - CurrentFuelIndex-6
		CurrentFuelValue := Round(SubStr(TelemetryData,CurrentFuelIndex+6,CurrentFuelIndexCount),1)
		CurrentFuelValueRounded := Round(CurrentFuelValue,1)
		FileDelete, %A_ScriptDir%\Files\FuelCurrent.txt
		FileAppend, %CurrentFuelValueRounded% l, %A_ScriptDir%\Files\FuelCurrent.txt
		
		;Maximum Fuel Capacity
		FuelCapacityIndex := InStr(TelemetryData,"fuelCapacity")
		FuelCapacityIndexEnd := InStr(TelemetryData,",",false,FuelCapacityIndex)
		FuelCapacityIndexCount := FuelCapacityIndexEnd - FuelCapacityIndex-14
		FuelCapacityValue := SubStr(TelemetryData,FuelCapacityIndex+14,FuelCapacityIndexCount)
		FuelCapacityValueRounded := Round(FuelCapacityValue,0)
		FileDelete, %A_ScriptDir%\Files\FuelMaxCapacity.txt
		FileAppend, %FuelCapacityValueRounded% l, %A_ScriptDir%\Files\FuelMaxCapacity.txt

		;Average Fuel Consumption
		FuelConsumptionIndex := InStr(TelemetryData,"fuelAverageConsumption")
		FuelConsumptionIndexEnd := InStr(TelemetryData,",",false,FuelConsumptionIndex)
		FuelConsumptionIndexCount := FuelConsumptionIndexEnd - FuelConsumptionIndex-24
		FuelConsumptionValue := SubStr(TelemetryData,FuelConsumptionIndex+24,FuelConsumptionIndexCount)
		FuelConsumptionValueMi := FuelConsumptionValue*1.60934
		FuelConsumptionValueRounded := Round(FuelConsumptionValue,2)
		FuelConsumptionValueMiRounded := Round(FuelConsumptionValueMi,2)
		FileDelete, %A_ScriptDir%\Files\FuelAverageConsumption.txt
		
		;Estimated Fuel Mileage
		FuelEstimatedMileage := Round(CurrentFuelValue/FuelConsumptionValue,1)
		FuelEstimatedMileageMi := Round((CurrentFuelValue/FuelConsumptionValue)*1.60934,1)
		
		FileDelete, %A_ScriptDir%\Files\FuelEstimatedMileage.txt
		if (MDistance = 1){
			;Kilometers
			FileAppend, %FuelConsumptionValueRounded% l/km, %A_ScriptDir%\Files\FuelAverageConsumption.txt
			FileAppend, %FuelEstimatedMileage% km, %A_ScriptDir%\Files\FuelEstimatedMileage.txt
		} else {
			;Miles
			FileAppend, %FuelConsumptionValueMiRounded% l/mi, %A_ScriptDir%\Files\FuelAverageConsumption.txt
			FileAppend, %FuelEstimatedMileageMi% mi, %A_ScriptDir%\Files\FuelEstimatedMileage.txt
		}

	}
	
	;ETA and Estimated Distance
	if CheckETA{
		;Estimated Time to Arrive
		estimatedTimeIndex := InStr(TelemetryData,"estimatedTime")
		estimatedTimeValue := SubStr(TelemetryData,estimatedTimeIndex+16,16)
		estimatedTimeHourValue := SubStr(TelemetryData,estimatedTimeIndex+27,2)
		estimatedTimeMinuteValue := SubStr(TelemetryData,estimatedTimeIndex+30,2)
		FileDelete, %A_ScriptDir%\Files\ETA.txt
		FileAppend, %estimatedTimeHourValue%:%estimatedTimeMinuteValue%, %A_ScriptDir%\Files\ETA.txt
		;Estimated Distance to Destination
		estimatedDistanceIndex := InStr(TelemetryData,"estimatedDistance")
		estimatedDistanceIndexEnd := InStr(TelemetryData,",",false,estimatedDistanceIndex)
		estimatedDistanceIndexCount := estimatedDistanceIndexEnd - estimatedDistanceIndex-19
		estimatedDistanceValueInKm := Round(SubStr(TelemetryData,estimatedDistanceIndex+19,estimatedDistanceIndexCount)/1000,1)
		estimatedDistanceValueInMiles := Round(estimatedDistanceValueInKm * 0.621371,1)
		FileDelete, %A_ScriptDir%\Files\EstimatedDistance.txt
		if (MDistance = 1){
			;Kilometers
			FileAppend, %estimatedDistanceValueInKm% km, %A_ScriptDir%\Files\EstimatedDistance.txt
		} else {
			;Miles
			FileAppend, %estimatedDistanceValueInMiles% mi, %A_ScriptDir%\Files\EstimatedDistance.txt
		}
	}
	
	;Truck and Trailer Damage
	if CheckTruckDamage{
		;Engine
		wearEngineIndex := InStr(TelemetryData,"wearEngine")
		wearEngineIndexEnd := InStr(TelemetryData,",",false,wearEngineIndex)
		wearEngineIndexCount := wearEngineIndexEnd - wearEngineIndex-12
		wearEngineValue := Round(SubStr(TelemetryData,wearEngineIndex+12,wearEngineIndexCount),0)
		;Transmission
		wearTransmissionIndex := InStr(TelemetryData,"wearTransmission")
		wearTransmissionIndexEnd := InStr(TelemetryData,",",false,wearTransmissionIndex)
		wearTransmissionIndexCount := wearTransmissionIndexEnd - wearTransmissionIndex-18
		wearTransmissionValue := Round(SubStr(TelemetryData,wearTransmissionIndex+18,wearTransmissionIndexCount),0)
		;Cabin
		wearCabinIndex := InStr(TelemetryData,"wearCabin")
		wearCabinIndexEnd := InStr(TelemetryData,",",false,wearCabinIndex)
		wearCabinIndexCount := wearCabinIndexEnd - wearCabinIndex-11
		wearCabinValue := Round(SubStr(TelemetryData,wearCabinIndex+11,wearCabinIndexCount),0)
		;Chassis
		wearChassisIndex := InStr(TelemetryData,"wearChassis")
		wearChassisIndexEnd := InStr(TelemetryData,",",false,wearChassisIndex)
		wearChassisIndexCount := wearChassisIndexEnd - wearChassisIndex-13
		wearChassisValue := Round(SubStr(TelemetryData,wearChassisIndex+13,wearChassisIndexCount),0)
		;Wheels
		wearWheelsIndex := InStr(TelemetryData,"wearWheels")
		wearWheelsIndexEnd := InStr(TelemetryData,",",false,wearWheelsIndex)
		wearWheelsIndexCount := wearWheelsIndexEnd - wearWheelsIndex-12
		wearWheelsValue := Round(SubStr(TelemetryData,wearWheelsIndex+12,wearWheelsIndexCount),0)
		;Trailer
		wearTrailerIndex := InStr(TelemetryData,"wear",true,wearWheelsIndex+12)
		wearTrailerIndexEnd := InStr(TelemetryData,",",false,wearTrailerIndex)
		wearTrailerIndexCount := wearTrailerIndexEnd - wearTrailerIndex-6
		wearTrailerValue := Round(SubStr(TelemetryData,wearTrailerIndex+6,wearTrailerIndexCount),0)

		
		;Getting the highest Truck damage value, as it should show ingame.
		MinMax(max:=false, values*) {
			for k, v in values
				if v is number
					x .= (k == values.MaxIndex() ? v : v ";")
			Sort, x, % "d`; N" (max ? " R" : "")
				RegExMatch(x, "[^\;]*", z) ; Alternative RegEx
			; RegExMatch(x, "[\d.-]*", z) ; Previous RegEx
			return z
		}
		wearHigher := Round(MinMax(True,wearWheelsValue,wearChassisValue,wearTransmissionValue,wearEngineValue),0)

		FileDelete, %A_ScriptDir%\Files\TruckDamageEngine.txt
		FileAppend, %wearEngineValue%, %A_ScriptDir%\Files\TruckDamageEngine.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageTransmission.txt
		FileAppend, %wearTransmissionValue%, %A_ScriptDir%\Files\TruckDamageTransmission.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageCabin.txt
		FileAppend, %wearCabinValue%, %A_ScriptDir%\Files\TruckDamageCabin.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageChassis.txt
		FileAppend, %wearChassisValue%, %A_ScriptDir%\Files\TruckDamageChassis.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageWheels.txt
		FileAppend, %wearWheelsValue%, %A_ScriptDir%\Files\TruckDamageWheels.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageHigher.txt
		FileAppend, %wearHigher%, %A_ScriptDir%\Files\TruckDamageHigher.txt
		FileDelete, %A_ScriptDir%\Files\TruckDamageTrailer.txt
		FileAppend, %wearTrailerValue%, %A_ScriptDir%\Files\TruckDamageTrailer.txt
	}

	;Data about Job
	if CheckJob{
	
		;Source City
		sourceCityIndex := InStr(TelemetryData,"sourceCity")
		sourceCityIndexEnd := InStr(TelemetryData,",",false,sourceCityIndex)
		sourceCityIndexCount := sourceCityIndexEnd - sourceCityIndex-14
		sourceCityValue := SubStr(TelemetryData,sourceCityIndex+13,sourceCityIndexCount)
		FileDelete, %A_ScriptDir%\Files\JobSourceCity.txt
		FileAppend, %sourceCityValue%, %A_ScriptDir%\Files\JobSourceCity.txt
		;Source Company
		sourceCompanyIndex := InStr(TelemetryData,"sourceCompany")
		sourceCompanyIndexEnd := InStr(TelemetryData,",",false,sourceCompanyIndex)
		sourceCompanyIndexCount := sourceCompanyIndexEnd - sourceCompanyIndex-17
		sourceCompanyValue := SubStr(TelemetryData,sourceCompanyIndex+16,sourceCompanyIndexCount)
		FileDelete, %A_ScriptDir%\Files\JobsourceCompany.txt
		FileAppend, %sourceCompanyValue%, %A_ScriptDir%\Files\JobSourceCompany.txt
		;Full Source
		FileDelete, %A_ScriptDir%\Files\JobSource.txt
		FileAppend, %sourceCityValue% - %sourceCompanyValue%, %A_ScriptDir%\Files\JobSource.txt

		;Destination City
		destinationCityIndex := InStr(TelemetryData,"destinationCity")
		destinationCityIndexEnd := InStr(TelemetryData,",",false,destinationCityIndex)
		destinationCityIndexCount := destinationCityIndexEnd - destinationCityIndex-19
		destinationCityValue := SubStr(TelemetryData,destinationCityIndex+18,destinationCityIndexCount)
		FileDelete, %A_ScriptDir%\Files\JobDestinationCity.txt
		FileAppend, %destinationCityValue%, %A_ScriptDir%\Files\JobDestinationCity.txt
		;Destination Company
		destinationCompanyIndex := InStr(TelemetryData,"destinationCompany")
		destinationCompanyIndexEnd := InStr(TelemetryData,",",false,destinationCompanyIndex)
		destinationCompanyIndexCount := destinationCompanyIndexEnd - destinationCompanyIndex-23
		destinationCompanyValue := SubStr(TelemetryData,destinationCompanyIndex+21,destinationCompanyIndexCount)
		FileDelete, %A_ScriptDir%\Files\JobdestinationCompany.txt
		FileAppend, %destinationCompanyValue%, %A_ScriptDir%\Files\JobDestinationCompany.txt
		;Full Destination
		FileDelete, %A_ScriptDir%\Files\JobDestination.txt
		FileAppend, %destinationCityValue% - %destinationCompanyValue%, %A_ScriptDir%\Files\JobDestination.txt
		
		;Cargo Name
		nameIndex := InStr(TelemetryData,"name",true)
		nameIndexEnd := InStr(TelemetryData,",",false,nameIndex)
		nameIndexCount := nameIndexEnd - nameIndex-8
		nameValue := SubStr(TelemetryData,nameIndex+7,nameIndexCount)
		FileDelete, %A_ScriptDir%\Files\JobCargo.txt
		FileAppend, %nameValue%, %A_ScriptDir%\Files\JobCargo.txt
		
		;Cargo Mass
		massIndex := InStr(TelemetryData,"mass")
		massIndexEnd := InStr(TelemetryData,",",false,massIndex)
		massIndexCount := massIndexEnd - massIndex-6
		massFullValue := SubStr(TelemetryData,massIndex+6,massIndexCount)
		massFullValueRound := Round(massFullValue,1)
		massValue := massFullValue / 1000
		massValueRound := (massValue,1)
		massValueLb := massFullValue * 2.20462
		massValueLbRound := Round(massValueLb,1)
		FileDelete, %A_ScriptDir%\Files\JobMass.txt
		if (MWeight = 1){
			;Kg
			FileAppend, %massFullValueRound% kg, %A_ScriptDir%\Files\JobMass.txt
		} else if (MWeight = 2){
			;Pounds
			FileAppend, %massValueLbRound% lb, %A_ScriptDir%\Files\JobMass.txt
		} else {
			;Tons
			FileAppend, %massValueRound% t, %A_ScriptDir%\Files\JobMass.txt
		}

		;Job Reward
		incomeIndex := InStr(TelemetryData,"income")
		incomeIndexEnd := InStr(TelemetryData,",",false,incomeIndex)
		incomeIndexCount := incomeIndexEnd - incomeIndex-8
		incomeValue := SubStr(TelemetryData,incomeIndex+8,incomeIndexCount)
		incomeValuePounds := incomeValue / 1.25
		incomeValuePoundsRound := Round(incomeValuePounds,0)
		
		FileDelete, %A_ScriptDir%\Files\JobReward.txt
		if (MCurrency = 1){
			;Euros
			FileAppend, %incomeValue%, %A_ScriptDir%\Files\JobReward.txt
		} else {
			;Pounds
			FileAppend, %incomeValuePoundsRound%, %A_ScriptDir%\Files\JobReward.txt
		}
		
		;Remaining Time
		remainingTimeIndex := InStr(TelemetryData,"remainingTime")
		remainingTimeValue := SubStr(TelemetryData,remainingTimeIndex+16,16)
		remainingTimeHourValue := SubStr(TelemetryData,remainingTimeIndex+27,2)
		remainingTimeMinuteValue := SubStr(TelemetryData,remainingTimeIndex+30,2)
		FileDelete, %A_ScriptDir%\Files\JobRemainingTime.txt
		FileAppend, %remainingTimeHourValue%:%remainingTimeMinuteValue%, %A_ScriptDir%\Files\JobRemainingTime.txt

	}
	
	;Odometer
	if CheckOdo{
		odometerIndex := InStr(TelemetryData,"odometer")
		odometerIndexEnd := InStr(TelemetryData,",",false,odometerIndex)
		odometerIndexCount := odometerIndexEnd - odometerIndex-10
		odometerValueInKm := Round(SubStr(TelemetryData,odometerIndex+10,odometerIndexCount),1)
		odometerValueInMiles := Round(odometerValueInKm * 0.621371,1)
		FileDelete, %A_ScriptDir%\Files\Odometer.txt
		if (MDistance = 1){
			;Kilometers
			FileAppend, %odometerValueInKm% km, %A_ScriptDir%\Files\Odometer.txt
		} else {
			;Miles
			FileAppend, %odometerValueInMiles% mi, %A_ScriptDir%\Files\Odometer.txt
		}
	}
	
	;Truck info
	if CheckTruck{
		;Truck Brand
		makeIndex := InStr(TelemetryData,"make")
		makeIndexEnd := InStr(TelemetryData,",",false,makeIndex)
		makeIndexCount := makeIndexEnd - makeIndex-8
		makeValue := SubStr(TelemetryData,makeIndex+7,makeIndexCount)
		FileDelete, %A_ScriptDir%\Files\TruckBrand.txt
		FileAppend, %makeValue%, %A_ScriptDir%\Files\TruckBrand.txt

		;Truck Model
		modelIndex := InStr(TelemetryData,"model")
		modelIndexEnd := InStr(TelemetryData,",",false,modelIndex)
		modelIndexCount := modelIndexEnd - modelIndex-9
		modelValue := SubStr(TelemetryData,modelIndex+8,modelIndexCount)
		FileDelete, %A_ScriptDir%\Files\TruckModel.txt
		FileAppend, %modelValue%, %A_ScriptDir%\Files\TruckModel.txt

		;Brand plus Model
		FileDelete, %A_ScriptDir%\Files\TruckBrandModel.txt
		FileAppend, %makeValue% %modelValue%, %A_ScriptDir%\Files\TruckBrandModel.txt
	}

	Loop, %loopdelaystep%{
		Sleep, 250
		GuiControl,, DelayBar, +1
		Gui, Submit, NoHide
		IniWrite, %MDistance%, %A_ScriptDir%\config.ini, Measurement, Distance
		IniWrite, %MWeight%, %A_ScriptDir%\config.ini, Measurement, Weight
		IniWrite, %MCurrency%, %A_ScriptDir%\config.ini, Measurement, Currency
	}
	
}

ButtonExit:
GuiClose:
if CheckDelete{
	FileRemoveDir, %A_ScriptDir%\Files\ ,1
}
ExitApp