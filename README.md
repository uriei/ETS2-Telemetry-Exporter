# ETS2 Telemetry Exporter
This app takes the data from the ETS2 Telemetry Server and prints it on text files.

Requires the use of https://github.com/Funbit/ets2-telemetry-server

This technically the first thing I coded, so it could be better.